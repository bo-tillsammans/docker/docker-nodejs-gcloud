# docker-nodejs-gcloud

https://hub.docker.com/r/cribcake/docker-nodejs-gcloud/tags

This is a Docker image for Node.js applications that uses Google Cloud Platform (GCP) services.
It includes the necessary dependencies and configurations to run your application on GCP.

Inspired from https://github.com/lakoo/lakoo-node-gcloud-docker, but with newer versions that we needed.

## How to run

Login to Docker Hub with: `docker login` (make sure you're part of the cribcake organization).

`go run main.go -version=<VERSION_NUMBER>`

### Multiple Docker accounts

If you have multiple docker accounts (i.e., one private and one from work), you need to crate a separate config.

First, create the config with the credentials you want to use: 

`docker --config ~/.docker/cribcake login -u <username> -p <password>`

Run the application with the custom config flag:

`go run main.go -version=<VERSION_NUMBER> -docker-config=~/.docker/cribcake`

From: https://stackoverflow.com/a/55635346/2986873
