package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"slices"
	"strings"
)

const (
	imageName           = "cribcake/docker-nodejs-gcloud"
	invalidImageVersion = "invalidImageVersion"
	emptyDockerConfig   = "emptyDockerConfig"
	envDockerCliHints   = "DOCKER_CLI_HINTS"
)

func main() {
	_ = os.Setenv(envDockerCliHints, "false")
	versionPtr := flag.String("version", invalidImageVersion, "version to use for Docker image")
	dockerConfigPtr := flag.String("docker-config", emptyDockerConfig, "[optional] custom Docker config file, relative path")
	debugPtr := flag.Bool("debug", false, "[optional] add debug flag to Docker")
	flag.Parse()
	if *versionPtr == invalidImageVersion {
		log.Fatalf("invalid version number!")
	}
	log.Printf("debug: %v\n", *debugPtr)
	if err := buildDockerImage(*versionPtr, *debugPtr); err != nil {
		log.Fatalf("could not build docker image, err: %v", err)
	}
	if err := pushDockerImage(*versionPtr, *dockerConfigPtr, *debugPtr); err != nil {
		log.Fatalf("could not push docker image, err: %v", err)
	}
	fmt.Printf("successfully build and pushed %s with version %s\n", imageName, *versionPtr)
	_ = os.Unsetenv(envDockerCliHints)
}

func buildDockerImage(version string, debug bool) error {
	args := []string{"buildx", "build", "--platform", "linux/amd64", "-t", fmt.Sprintf("%s:%s", imageName, version), "."}
	if debug {
		args = slices.Insert(args, 2, "--debug", "--no-cache")
	}
	log.Printf("args: %v\n", args)
	cmd := exec.Command("docker", args...)
	return startCmd(cmd)
}

func pushDockerImage(version, dockerConfigPath string, debug bool) error {
	args := []string{}
	if dockerConfigPath != "" {
		args = append(args, "--config", dockerConfigPath)
	}
	if debug {
		args = append(args, "--debug")
	}
	args = append(args, "push", fmt.Sprintf("%s:%s", imageName, version))
	cmd := exec.Command("docker", args...)
	return startCmd(cmd)
}

// startCmd starts the provided command and returns any error
func startCmd(cmd *exec.Cmd) error {
	stderr, _ := cmd.StderrPipe()
	if err := cmd.Start(); err != nil {
		return err
	}

	scanner := bufio.NewScanner(stderr)
	for scanner.Scan() {
		validText := strings.HasPrefix(scanner.Text(), "#") || strings.Contains(scanner.Text(), "successfully build")
		if scanner.Text() != "" && !validText {
			return errors.New(scanner.Text())
		}
		fmt.Println(scanner.Text())
	}
	return nil
}
